class TotalCostPromotion < Promotion

  def initialize(offset, discount)
    @offset = offset * 100
    @discount = discount
  end

  def total_cost_type?
    true
  end

  def apply(total)
    total -= @discount * total if total >= @offset
    total
  end

end
