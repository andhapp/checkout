class ItemPromotion < Promotion

  def initialize(item, count_offset, new_cost)
    @item = item
    @count_offset = count_offset
    @new_cost = new_cost * 100
  end

  def apply(basket, total)
    count = basket[@item]
    if count && count >= @count_offset
      total += @new_cost * count
      basket.delete(@item)
    end
    [basket, total]
  end

end
