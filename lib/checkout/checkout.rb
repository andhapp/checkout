class Checkout

  def initialize(promotions)
    @total_cost_promotions = promotions.select {|promotion| promotion.total_cost_type? }
    @item_promotions = promotions - @total_cost_promotions
    @basket = {}
  end

  def scan(item)
    @basket[item] ||= 0
    @basket[item] += 1
  end

  def total
    total = 0

    basket = @basket.dup
    @item_promotions.each do |promotion|
      basket, total = promotion.apply(basket, total)
    end

    basket.each do |item, count|
      total += item.cost_in_units * count
    end

    @total_cost_promotions.each do |promotion|
      total = promotion.apply(total)
    end

    "£#{total.round/100.0}"
  end

end
