class Item

  attr_reader :code, :cost_in_units

  def initialize(code, cost)
    @code = code
    @cost_in_units = cost * 100
  end

end
