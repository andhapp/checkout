require "spec_helper"

describe Item do

  context "#initialize" do

    it "accepts two parameters" do
      expect { 
        Item.new("001", 10.00) 
      }.to_not raise_error
    end

  end

  it "converts the cost into pence" do
    item = Item.new("001", 10.00) 
    item.cost_in_units.should == 1000
  end

  it "returns the code string" do
    item = Item.new("001", 10.00) 
    item.code.should == "001"
  end

end
