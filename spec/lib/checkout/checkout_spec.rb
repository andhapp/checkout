require "spec_helper"

describe Checkout do

  let(:item1) { Item.new("001", 9.25) }
  let(:item2) { Item.new("002", 45.00) }
  let(:item3) { Item.new("003", 19.95) }

  let(:checkout) { Checkout.new([]) }

  describe "No promotions" do

    context "when scanning single item" do

      it "should calculate the total" do
        checkout.scan(item1)

        checkout.total.should == "£9.25"
      end

    end

    context "when scanning multiple items" do

      it "should calculate the total" do
        checkout.scan(item1)
        checkout.scan(item2)

        checkout.total.should == "£54.25"
      end

    end

    context "when scanning same item multiple items" do

      it "should calculate the total" do
        checkout.scan(item2)
        checkout.scan(item2)
        checkout.scan(item1)

        checkout.total.should == "£99.25"
      end

    end

  end

  describe "Promotions" do

    let(:checkout) { Checkout.new(promotions) }

    context "for discount on total over a certain amount" do

      let(:promotions) do
        [TotalCostPromotion.new(60.00, 0.1)]
      end

      it "should calculate the total" do
        checkout.scan(item1)
        checkout.scan(item2)
        checkout.scan(item3)

        checkout.total.should == "£66.78"
      end

    end

    context "for discount on cost of a certain product" do

      let(:promotions) do
        [ItemPromotion.new(item1, 2, 8.50)]
      end

      it "should calculate the total" do
        checkout.scan(item1)
        checkout.scan(item3)
        checkout.scan(item1)

        checkout.total.should == "£36.95"
      end

    end

    context "for discount on cost of a certain product and total over certain amount" do

      let(:promotions) do
        [TotalCostPromotion.new(60.00, 0.1),
         ItemPromotion.new(item1, 2, 8.50)]
      end

      it "should calculate the total" do
        checkout.scan(item1)
        checkout.scan(item2)
        checkout.scan(item1)
        checkout.scan(item3)

        checkout.total.should == "£73.76"
      end

    end

  end

end
