require "spec_helper"

describe ItemPromotion do

  context "#initialize" do

    it "accepts three parameters" do
      expect { 
        ItemPromotion.new(stub, 2, 8.50) 
      }.to_not raise_error
    end

  end
 
  context "#apply" do

    let(:item1) { Item.new("001", 9.50) }

    context "Item count is more than offset" do

      it "should apply the correct discount" do
        total = 0
        basket = {item1 => 2}
        ip = ItemPromotion.new(item1, 2, 8.50)

        ip.apply(basket, total).should == [{}, 1700]
      end

    end

    context "Item count is less than offset" do

      it "should apply the correct discount" do
        total = 0
        basket = {item1 => 1}

        ip = ItemPromotion.new(item1, 2, 8.50)

        ip.apply(basket, total).should == [{item1 => 1}, 0]
      end

    end

  end
end
