require "spec_helper"

describe TotalCostPromotion do

  context "#initialize" do

    it "accepts two parameters" do
      expect { 
        TotalCostPromotion.new(60.00, 0.1) 
      }.to_not raise_error
    end

  end
 
  context "#apply" do

    context "total is more than offset" do

      it "should apply the correct discount" do
        tcp = TotalCostPromotion.new(60.00, 0.1)
        
        tcp.apply(7000).should == 6300
      end

    end

    context "total is less than offset" do

      it "should apply the correct discount" do
        tcp = TotalCostPromotion.new(60.00, 0.1)
        
        tcp.apply(5900).should == 5900
      end

    end

  end

end
