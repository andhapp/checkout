# Checkout

## Environment

* MacVim
* Ruby 2
* Rspec


## Approach

* This was developeed in a TDD way. Started with a spec and went through
Red-Green-Refactor cycle.

* All costs are converted into pence to avoid losing any data in
conversion from float to int. Also, this information is hidden from the
user's creating the items because in an ideal world you don't want them
to convert these things. It's easy, yet can lead to minor errors.

* In addition to the interface give, here's the interface to create the
promotions and items:

Item:

<pre>
  # @param [String] item code
  # @param [Double] item cost

  Item.new("001", 9.25)
</pre>

ItemPromotion:

<pre>
  # @param [Item] Item the promotion is for
  # @param [Integer] Count offset post which the new cost applies
  # @param [Double] New cost

  ItemPromotion.new(item1, 2, 8.50)
</pre>

TotalCostPromotion:

<pre>
  # @param [Double] Total offset post which discount applies
  # @param [Double] Discount

  TotalCostPromotion.new(60.00, 0.1)
</pre>

* At the moment, it assumes that there's always a percentage discount on
thte total cost and the same goes for item discounts. To mitigate this,
we could introduce a discount class that will determine the kind of
discount.
